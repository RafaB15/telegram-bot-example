@awards
Feature: Awards

  Scenario: A1 - Awards received when we query a movie with awards
    Given /awards "Titanic" is sent
    Then I should get "The movie Titanic Won 11 Oscars. 126 wins & 83 nominations total"

  Scenario: A2 - Awards of all movies received when we query multiple movies with awards
    Given /awards "Titanic, Casablanca" is sent
    Then I should get "The movie Titanic Won 11 Oscars. 126 wins & 83 nominations total"
    And I should get "The movie Casablanca Won 3 Oscars. 14 wins & 11 nominations total"

  Scenario: A3 - A message saying that the movie could not be found received when we query an unknown movie
    Given /awards "Peliculonlonlon" is sent
    Then I should get "I'm sorry, but I don't seem to have information about this movie at the moment"

  Scenario: A4 - A message saying that a movie has no awards in received when we query a movie with no awards
    Given /awards "Metegol" is sent
    Then I should get "The movie Metegol hasn't won any awards"