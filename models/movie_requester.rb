class MovieRequester
  def initialize(api_key)
    @api_key = api_key
  end

  def request_movie(movie_name)
    api_url = 'https://www.omdbapi.com/'
    response = Faraday.get(api_url, { t: movie_name, apikey: @api_key })
    response_json = JSON.parse(response.body)
    raise MovieNotFoundError, movie_name if response_json.key?('Error') && response_json['Error'] == 'Movie not found!'

    response_json
  end
end

class MovieNotFoundError < StandardError
  def initialize(movie_name)
    super("Movie '#{movie_name}' not found.")
  end
end
