require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_texts(token, message_texts)
  message_texts.each do |message_text|
    then_i_get_text(token, message_text)
  end
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

# Stub for the API call to get the awards of the movie Titanic
def listen_for_awards_titanic
  api_ombd_stub_body = {
    "Title": 'Titanic',
    "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Titanic')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body, headers: {})
end

def listen_for_awards_titanic_and_casablanca
  api_ombd_stub_body_titanic = {
    "Title": 'Titanic',
    "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total'
  }.to_json

  api_ombd_stub_body_casablanca = {
    "Title": 'Casablanca',
    "Awards": 'Won 3 Oscars. 14 wins & 11 nominations total'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Titanic')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body_titanic, headers: {})

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Casablanca')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body_casablanca, headers: {})
end

def listen_for_awards_peliculonlonlon
  api_ombd_stub_body = {
    "Error": 'Movie not found!'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Peliculonlonlon')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body, headers: {})
end

def listen_for_awards_metegol
  api_ombd_stub_body = {
    "Title": 'Metegol',
    "Awards": 'N/A'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey&t=Metegol')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body, headers: {})
end

describe 'BotClient' do
  it 'should get a /version message and respond with current version' do
    token = 'fake_token'

    when_i_send_text(token, '/version')
    then_i_get_text(token, Version.current)

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    token = 'fake_token'

    when_i_send_text(token, '/say_hi Emilio')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    token = 'fake_token'

    when_i_send_text(token, '/start')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /stop message and respond with Chau' do
    token = 'fake_token'

    when_i_send_text(token, '/stop')
    then_i_get_text(token, 'Chau, egutter')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    token = 'fake_token'

    when_i_send_text(token, '/tv')
    then_i_get_keyboard_message(token, 'Quien se queda con el trono?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    token = 'fake_token'

    when_i_send_keyboard_updates(token, 'Quien se queda con el trono?', '2')
    then_i_get_text(token, 'A mi también me encantan los dragones!')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    token = 'fake_token'

    when_i_send_text(token, '/unknown')
    then_i_get_text(token, 'Uh? No te entiendo! Me repetis la pregunta?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /awards <movie> message and respond with the information of the movie' do
    listen_for_awards_titanic
    when_i_send_text('fake_token', '/awards Titanic')
    then_i_get_text('fake_token', 'The movie Titanic Won 11 Oscars. 126 wins & 83 nominations total')
    app = BotClient.new('fake_token')

    app.run_once
  end

  it 'should get a /awards <movie1>, <movie2> message and respond with the information of the movies' do
    listen_for_awards_titanic_and_casablanca
    when_i_send_text('fake_token', '/awards Titanic, Casablanca')
    then_i_get_texts('fake_token', ['The movie Titanic Won 11 Oscars. 126 wins & 83 nominations total', 'The movie Casablanca Won 3 Oscars. 14 wins & 11 nominations total'])
    app = BotClient.new('fake_token')
    app.run_once
  end

  it 'should get a /awards <unknown_movie> message and respond with an appropriate message' do
    listen_for_awards_peliculonlonlon
    when_i_send_text('fake_token', '/awards Peliculonlonlon')
    then_i_get_text('fake_token', "I'm sorry, but I don't seem to have information about this movie at the moment")
    app = BotClient.new('fake_token')
    app.run_once
  end

  it 'should get a /awards <movie_with_no_awards> message and respond with an appropriate message' do
    listen_for_awards_metegol
    when_i_send_text('fake_token', '/awards Metegol')
    then_i_get_text('fake_token', 'The movie Metegol hasn\'t won any awards')
    app = BotClient.new('fake_token')
    app.run_once
  end
end
