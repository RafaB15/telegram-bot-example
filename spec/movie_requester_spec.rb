require 'spec_helper'
require 'web_mock'

require "#{File.dirname(__FILE__)}/../models/movie_requester"

def when_i_request_the_movie_titanic
  api_ombd_stub_body = {
    "Title": 'Titanic',
    "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey=fake_token&t=Titanic')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body, headers: {})
end

def when_i_request_the_movie_peliculonlonlon
  api_ombd_stub_body = {
    "Error": 'Movie not found!'
  }.to_json

  stub_request(:get, 'https://www.omdbapi.com/?apikey=fake_token&t=Peliculonlonlon')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_ombd_stub_body, headers: {})
end

describe MovieRequester do
  describe '#request_movie' do
    it 'returns the movie information' do
      when_i_request_the_movie_titanic

      movie_requester = described_class.new('fake_token')
      response = movie_requester.request_movie('Titanic')

      expect(response['Title']).to eq('Titanic')
      expect(response['Awards']).to eq('Won 11 Oscars. 126 wins & 83 nominations total')
    end

    it 'raises an error when the movie is not found' do
      when_i_request_the_movie_peliculonlonlon
      movie_requester = described_class.new('fake_token')
      expect { movie_requester.request_movie('Peliculonlonlon') }.to raise_error(MovieNotFoundError, "Movie 'Peliculonlonlon' not found.")
    end
  end
end
